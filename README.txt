Note: This is not ready for anything and is totally connected to a specific project I'm working on.
To see it in action, you'll need to:
* create a voting_rules exposed Rule on a vote and Action to call the included function (_rate_referenced_cascade_node_vote_to_terms)
* change the hardcoded field names implemented to fit your project

I would love to know if this is of use to you and to have help with:
* codifying the rule/action hooks
* admin interface to configure fields
* decoupling hardcoded fieldnames from code
* decoupling hardcoded rate field assumption(s)
* decoupling assumption that user is using Node and Taxonomy Term fields
